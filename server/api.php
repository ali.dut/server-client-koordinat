<?php
include "config.php";


spl_autoload_register(function ($class) {
    require __DIR__ . "/src/$class.php";
});

set_error_handler("ErrorHandler::handleError");
set_exception_handler("ErrorHandler::handleException");



$parts = explode("/", $_SERVER["REQUEST_URI"]);

if( $parts[3] !="api"){
    include "index.php";
    exit;
}
header("Content-type: application/json; charset=UTF-8");
if ($parts[4] != "koordinat") {
    echo $parts[4]."sayfasi bulunamadi";

    http_response_code(404);
    exit;
}



$id = $parts[5] ?? null;

$database = new Database("localhost", "koordinat", "root", "");

$gateway = new KoordinatGateway($database);

$controller = new KoordinatController($gateway);

$controller->processRequest($_SERVER["REQUEST_METHOD"], $id);
