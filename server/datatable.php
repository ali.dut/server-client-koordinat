<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<div style="margin-top: 20px;">
    <h1>Yapılan işlemler</h1>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>X</th>
                <th>Y</th>
                <th>Z</th>
                <th>TARİH</th>
            </tr>
        </thead>
    </table>
</div>

<script>
    $(document).ready(function() {
        var table = $('#example').DataTable({
            'columns': [
                {
                    data: 'id'
                },
                {
                    data: 'x'
                },
                {
                    data: 'y'
                },
                {
                    data: 'z'
                },
                {
                    data: 'time'
                },
            ],
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "visible": false,
                    "targets": [0]
                },
            ],
            "responsive": true,
            "language": {
                "emptyTable": "Gösterilecek ver yok.",
                "processing": "Veriler yükleniyor",
                "sDecimal": ".",
                "sInfo": "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                "sInfoFiltered": "(_MAX_ kayıt içerisinden bulunan)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Sayfada _MENU_ kayıt göster",
                "sLoadingRecords": "Yükleniyor...",
                "sSearch": "Ara:",
                "sZeroRecords": "Eşleşen kayıt bulunamadı.",
                "oPaginate": {
                    "sFirst": "İlk",
                    "sLast": "Son",
                    "sNext": "Sonraki",
                    "sPrevious": "Önceki"
                },
                "oAria": {
                    "sSortAscending": ": artan sütun sıralamasını aktifleştir",
                    "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                },
                "select": {
                    "rows": {
                        "_": "%d kayıt seçildi",
                        "0": "",
                        "1": "1 kayıt seçildi"
                    }
                }
            }
        });
        setInterval(function(){
                    gecmisKayitlar(table);
                },1000);
    });

    function gecmisKayitlar(table)
    {
        $.ajax({
            type: 'GET',
            url: '<?php echo BASEURL?>/api/koordinat',
            success: function(data) {
                table.clear();
                table.rows.add( data ).draw();
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
</script>