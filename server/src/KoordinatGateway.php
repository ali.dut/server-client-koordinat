<?php
date_default_timezone_set('Europe/Istanbul');
class KoordinatGateway
{
    private PDO $conn;
    
    public function __construct(Database $database)
    {
        $this->conn = $database->getConnection();
    }
    
    public function getAll(): array
    {
        $sql = "SELECT *
                FROM tb_koordinat ORDER BY id DESC";
                
        $stmt = $this->conn->query($sql);
        
        $data = [];
        
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            
            $data[] = $row;
        }
        
        return $data;
    }

    public function getLast(): array
    {
        $sql = "SELECT *
                FROM tb_koordinat ORDER BY id DESC LIMIT 0,1";
                
        $stmt = $this->conn->query($sql);
        
        $data = [];
        
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            
            $data[] = $row;
        }
        
        if(empty($data))
            $data[] = ["x"=> 0,"y"=> 0,"z"=> 0];
        
        return $data;
    }
    
    public function create(array $data): string
    {
        $sql = "INSERT INTO tb_koordinat (x, y, z, time)
                VALUES (:x, :y, :z, :time)";
                
        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindValue(":x", $data["x"] ?? 0, PDO::PARAM_INT);
        $stmt->bindValue(":y", $data["y"] ?? 0, PDO::PARAM_INT);
        $stmt->bindValue(":z", $data["z"] ?? 0, PDO::PARAM_INT);
        $stmt->bindValue(":time", date("Y-m-d H:i:s"));
        
        $stmt->execute();
        
        return $this->conn->lastInsertId();
    }
    
    public function get(string $id): array | false
    {
        $sql = "SELECT *
                FROM tb_koordinat
                WHERE id = :id";
                
        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        
        $stmt->execute();
        
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
        return $data;
    }
    
    public function update(array $current, array $new): int
    {
        $sql = "UPDATE tb_koordinat
                SET x = :x, y = :y, z = :z, time= :time
                WHERE id = :id";
                
        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindValue(":x", $new["x"] ?? $current["x"], PDO::PARAM_INT);
        $stmt->bindValue(":y", $new["y"] ?? $current["y"], PDO::PARAM_INT);
        $stmt->bindValue(":z", $new["z"] ?? $current["z"], PDO::PARAM_INT);
        $stmt->bindValue(":time", date("Y-m-d H:i:s"));
        $stmt->bindValue(":id", $current["id"], PDO::PARAM_INT);
        
        $stmt->execute();
        
        return $stmt->rowCount();
    }
    
    public function delete(string $id): int
    {
        $sql = "DELETE FROM tb_koordinat
                WHERE id = :id";
                
        $stmt = $this->conn->prepare($sql);
        
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        
        $stmt->execute();
        
        return $stmt->rowCount();
    }
}











