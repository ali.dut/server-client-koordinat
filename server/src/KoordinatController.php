<?php

class KoordinatController
{
    public function __construct(private KoordinatGateway $gateway)
    {
    }
    
    public function processRequest(string $method, ?string $id): void
    {
        if ($id) {
            
            $this->processResourceRequest($method, $id);
            
        } else {
            
            $this->processCollectionRequest($method);
            
        }
    }
    
    private function processResourceRequest(string $method, string $id): void
    {

        if ($id == "last")
        {
            $rows = $this->gateway->getLast();
            echo json_encode([
                "message" => "Son kayıt",
                "rows" => $rows
            ]);
            return;
        }

        $koordinat = $this->gateway->get($id);
        
        if ( ! $koordinat) {
            http_response_code(404);
            echo json_encode(["message" => "Koordinat Bulunamadı"]);
            return;
        }


        switch ($method) {
            case "GET":
                echo json_encode($koordinat);
                break;
                
            case "PUT":
                $data = (array) json_decode(file_get_contents("php://input"), true);
                
                $rows = $this->gateway->update($koordinat, $data);
                
                echo json_encode([
                    "message" => "Koordinat $id güncellendi",
                    "rows" => $rows
                ]);
                break;
                
            case "DELETE":
                $rows = $this->gateway->delete($id);
                
                echo json_encode([
                    "message" => "Koordinat $id silindi",
                    "rows" => $rows
                ]);
                break;
                
            default:
                http_response_code(405);
                header("Allow: GET, PATCH, DELETE");
        }
    }
    
    private function processCollectionRequest(string $method): void
    {
        switch ($method) {
            case "GET":
                echo json_encode($this->gateway->getAll());
                break;
                
            case "POST":
                $data = (array) json_decode(file_get_contents("php://input"), true);
                $id = $this->gateway->create($data);
                http_response_code(201);
                echo json_encode([
                    "message" => "Koordinat Oluşturuldu",
                    "id" => $id
                ]);
                break;
            
            default:
                http_response_code(405);
                header("Allow: GET, POST");
        }
    }
    

}









