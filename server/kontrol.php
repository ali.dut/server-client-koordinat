<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <input type="text" id="txtx" name="txtx" class="form-control" value="0" readonly>
            </div>
            <div class="col-md-4">
                <input type="text" id="txty" name="txty" class="form-control" value="0" readonly>
            </div>
            <div class="col-md-4">
                <input type="text" id="txtz" name="txtz" class="form-control" value="0" readonly>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <h3>X setting</h3>
        <button type="submit" id="sol" name="sol" class="btn btn-danger btn-block" style="margin-top: 30px;" onclick="azalt('x')">SOL</button>
        <button type="submit" id="sag" name="sag" class="btn btn-success btn-block" style="margin-top: 30px;" onclick="arttir('x')">SAĞ</button>
    </div>
    <div class="col-md-4">
        <h3>Y setting</h3>
        <button type="submit" id="geri" name="geri" class="btn btn-danger btn-block" style="margin-top: 30px;" onclick="azalt('y')">GERİ</button>
        <button type="submit" id="ileri" name="ileri" class="btn btn-success btn-block" style="margin-top: 30px;" onclick="arttir('y')">İLERİ</button>
    </div>
    <div class="col-md-4">
        <h3>Z setting</h3>
        <button type="submit" id="asagi" name="asagi" class="btn btn-danger btn-block" style="margin-top: 30px;" onclick="azalt('z')">AŞAĞI</button>
        <button type="submit" id="yukari" name="yukari" class="btn btn-success btn-block" style="margin-top: 30px;" onclick="arttir('z')">YUKARI</button>
    </div>
</div>
<script>
    function azalt(k) {
        var sayi = $("#txt" + k).val();
        var b = parseInt(sayi);
        b = b - 1;
        $("#txt" + k).val(b);
        kaydet();
    }

    function arttir(k) {
        var sayi = $("#txt" + k).val();
        var b = parseInt(sayi);
        b = b + 1;
        $("#txt" + k).val(b);
        kaydet();
    }

    function kaydet() {
        var x = $("#txtx").val();
        var y = $("#txty").val();
        var z = $("#txtz").val();
        var data = '{"x":' + x + ',"y":' + y + ',"z":' + z + '}';
        $.ajax({
            type: 'POST',
            url: '<?php echo BASEURL?>/api/koordinat',
            data: data, // or JSON.stringify ({name: 'jonas'}),
            success: function(data) {

            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
</script>

<script>
    $(document).ready(function(){
        sonKayit();
    });

    function sonKayit()
    {
        $.ajax({
            type: 'GET',
            url: '<?php echo BASEURL?>/api/koordinat/last',
            success: function(data) {
                $("#txtx").val(data.rows[0].x);
                $("#txty").val(data.rows[0].y);
                $("#txtz").val(data.rows[0].z);
            },
            contentType: "application/json",
            dataType: 'json'
        });
    }
</script>